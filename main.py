# pylint: disable-msg=C0103
def search_algorithm(x, y, Binary_matrix):
    length = len(Binary_matrix)
    width = len(Binary_matrix[0])
    if x == 0 or x == length - 1 or y == 0 or y == width - 1:
        if Binary_matrix[x][y] == 1:
            return [(x, y)]
    if Binary_matrix[x][y] != 1:
        return []
    Binary_matrix[x][y] = 8
    for i in [[x - 1, y], [x + 1, y], [x, y - 1], [x, y + 1]]:
        result = search_algorithm(i[0], i[1], Binary_matrix)
        temp = len(result)
        if temp > 0:
            result.append((x, y))
            return result
    return []


def help_function(pozition, Binary_matrix):
    x = pozition[0]
    y = pozition[1]
    length = len(Binary_matrix)
    width = len(Binary_matrix[0])
    if x == 0 or x == length - 1 or y == 0 or y == width - 1:
        if Binary_matrix[x][y] == 1:
            return 0
    res = len(search_algorithm(x, y, Binary_matrix))
    if res == 0:
        answer = - 1
    else:
        answer = res - 1
    return answer


def output_maze(matrix_maze):
    for i in matrix_maze:
        print(i)


def run(pozition, Binary_matrix):
    res = help_function(pozition, Binary_matrix)
    return res
